import * as firebase from 'firebase';

export function firebaseInit() {
    const config = {
        apiKey: "AIzaSyD9QwR1Tj0UrPcmaSdw4Qj8V5dAMSEr_Y4",
        authDomain: "sample-dahsboard.firebaseapp.com",
        databaseURL: "https://sample-dahsboard.firebaseio.com",
        projectId: "sample-dahsboard",
        storageBucket: "sample-dahsboard.appspot.com",
        messagingSenderId: "497225143941"
    };

    firebase.initializeApp(config);
}