import React            from 'react';
import ReactDOM         from 'react-dom';
import {BrowserRouter}  from 'react-router-dom';
import App              from './components/App';
import {firebaseInit}   from './firebase/index';
import './css/index.css';

firebaseInit();

ReactDOM.render(
    <BrowserRouter>
        <App />
    </BrowserRouter>,
    document.getElementById('root')
);