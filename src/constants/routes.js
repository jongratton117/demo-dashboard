export const LANDING     = "/";
export const DASHBOARD   = "/dashboard";
export const DEMO_UI     = "/dashboard";
export const RTDBDEMO    = "/dashboard/rtdb-demo";
export const CREATE_USER = "/dashboard/rtdb-demo/create-user";
export const EDIT_USER   = '/dashboard/rtdb-demo/edit-user/:id';

export function makeCreateUserPath(id) {
    return `/dashboard/rtdb-demo/edit-user/${id}`;
}