import React, { Component } from 'react';
import {Route}              from 'react-router-dom';
import * as ROUTES          from '../constants/routes';

//Styles
import '../sass/App.scss';

//Components
import Landing              from './landing';
import Dashboard            from './dashboard/dashboard';

//End Imports

class App extends Component {
  render() {
    return (
      <div className="App">
          <Route exact path={ROUTES.LANDING}    component={Landing} />
          <Route path={ROUTES.DASHBOARD}        component={Dashboard} />
      </div>
    );
  }
}

export default App;