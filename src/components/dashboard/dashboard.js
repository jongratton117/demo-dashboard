import React        from 'react';
import {Route}      from 'react-router-dom';
import * as ROUTES  from '../../constants/routes';

//Blocks
import Navigation   from './blocks/navigation';
import Sidebar      from './blocks/sidebar';

//Components
import DemoDashboardUI      from './views/demoDashboardUI';
import RtdbDemo     from './views/rtdbDemo';
import CreateUser   from './views/createUser';
import EditUser     from './views/editUser';

class Dashboard extends React.Component {
    render() {
        return (
            <div id="home" className="d-flex flex-column h-100">
                <Navigation />
                <div className="container-fluid flex-fill d-flex">
                    <div className="row flex-fill">
                        <Sidebar />
                        <div id="content" className="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 py-3">
                            <Route exact path={ROUTES.DEMO_UI}      component={DemoDashboardUI} />
                            <Route exact path={ROUTES.RTDBDEMO}     component={RtdbDemo} />
                            <Route exact path={ROUTES.CREATE_USER}  component={CreateUser} />
                            <Route path={ROUTES.EDIT_USER}          component={EditUser} />
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Dashboard;