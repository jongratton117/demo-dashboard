import React        from 'react';
import {NavLink,
        Link}       from 'react-router-dom';
import * as ROUTES  from '../../../constants/routes';

class Sidebar extends React.Component {
    render() {
        return (
            <div className="col-sm-3 col-md-2 sidebar bg-dark h-100" id="sidebar">
                <div className="guest-user">
                    <i className="fas fa-user" />
                    <div className="dropdown">
                        <a className="btn btn-primary btn-sm dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            GUEST USER
                        </a>
                        <div className="dropdown-menu" aria-labelledby="dropdownMenuLink">
                            <a className="dropdown-item" href="#">Profile</a>
                            <Link to="/" className="dropdown-item sign-out" >
                                <i className="fas fa-sign-out-alt"></i> Sign Out
                            </Link>
                        </div>
                    </div>
                </div>
                <ul className="nav flex-column">
                    <li>
                        <NavLink to={ROUTES.DASHBOARD} exact className="nav-link" activeClassName="active">
                            <i className="fas fa-tachometer-alt" /> DASHBOARD
                        </NavLink>
                    </li>
                    <li>
                        <NavLink to={ROUTES.RTDBDEMO} className="nav-link ">
                            <i className="fas fa-database" /> USER MANAGEMENT
                        </NavLink>
                    </li>

                </ul>
            </div>
        )
    }
}

export default Sidebar;