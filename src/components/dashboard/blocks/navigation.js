import React    from 'react';
import {Link}   from 'react-router-dom';
import * as ROUTES from '../../../constants/routes';
const logo = require('../../../images/dashboard-demo.png');

class Navigation extends React.Component {
    render() {
        return (
            <nav className="navbar navbar-expand-lg navbar-light">
                <Link to={ROUTES.DASHBOARD} className="navbar-brand">
                    <img src={logo} alt=""/>
                </Link>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse ml-auto" id="navbarSupportedContent">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item mr-4">
                            <a href="#" className="nav-link">
                                <span className="d-lg-none d-xl-none mr-3">Notifications </span><i className="far fa-bell with-notification"/>
                            </a>
                        </li>
                        <li className="nav-item mr-4">
                            <a href="#" className="nav-link">
                                <i className="fas fa-question-circle"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        )
    }
}

export default Navigation;

