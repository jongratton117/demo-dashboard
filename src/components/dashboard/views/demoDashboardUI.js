import React            from 'react';
import * as firebase    from 'firebase';

import StatCard         from './elements/statCard';
import WorldMap         from './elements/worldMap';
import Cities           from './elements/cities';
import StatUpdater      from './elements/statUpdater';
import CityUpdater      from './elements/cityUpdater';

const loading = require('../../../images/loading.svg');

class DemoDashboardUI extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            receivedData: null
        }
    }

    componentDidMount() {
        const dbRefObject = firebase.database().ref().child('sample_data');
        dbRefObject.on('value', snap => {
            this.setState({
                receivedData: true,
                countries: snap.val().countries,
                stats: snap.val().stats
            })
        });
    }

    render() {
        console.log(this.state)
        return (
            <div id="dashboard-welcome" className="container-fluid">
                <h1 className="section-header">Example Dashboard UI</h1>
                <p className="sub-header">An example of a dashboard UI that pulls data from Firebase Realtime Database.</p>
                {this.state.receivedData ? (
                    <div>
                        <div className="row">
                            <StatCard title="Total Sales" value={this.state.stats.total_sales} iconClass="fa-money-bill-wave" color="warning" />
                            <StatCard title="Products" value={this.state.stats.products} iconClass="fa-box-open" color="info" />
                            <StatCard title="Orders" value={this.state.stats.orders} iconClass="fa-coins" color="success" />
                        </div>
                        <div className="row mt-5">
                            <WorldMap />
                            <Cities data={this.state.countries} />
                        </div>
                        <h4 className="mt-5">Update Values</h4>
                        <p>Push new data to the database and watch it update above.</p>
                        <div className="row mt-3">
                            <StatUpdater />
                            <CityUpdater />
                        </div>
                    </div>

                ) : (
                    <img className="loading-icon" src={loading} alt="" />
                )}

            </div>
        )
    }
}

export default DemoDashboardUI;