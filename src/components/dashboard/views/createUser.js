import React            from 'react';
import * as firebase    from 'firebase';
import {Redirect}       from 'react-router-dom';
import * as ROUTES      from '../../../constants/routes';

class CreateUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            validation: null,
            redirect: false
        }
        this.onChangeHandler    = this.onChangeHandler.bind(this);
        this.createRtdbRecord   = this.createRtdbRecord.bind(this);
    }

    onChangeHandler(e) {
        console.log(e.target.value);
        let name = e.target.name;
        let val = e.target.value;
        this.setState({
            [name]: val
        })
    }

    async createRtdbRecord(e) {

        // Quick method for checking form validation using inbuilt browser methods. This would normally use a custom validation method and pre-submission confirmation.
        e.preventDefault();
        const form = document.getElementById("create-user-form")
        form.checkValidity();
        form.reportValidity();

        if(form.reportValidity() === false)
            this.setState({
                validation: "failed"
            })
        else {
            this.setState({
                validation: "passed"
            })
            await firebase.database().ref('users/').push({
                name: this.state.name,
                surname: this.state.surname,
                gender: this.state.gender,
                age: this.state.age
            });
            this.setState({
                redirect: true
            })
        }
    }

    render() {
        const redirect = this.state.redirect;
        if(redirect === true)
            return <Redirect to={ROUTES.RTDBDEMO} />
        return (
            <div id="fb-rtdb-page" className="container-fluid">
                <h1 className="section-header">Create New User</h1>
                <p>Push new user data from this app to the Google Firebase</p>
                <div className="card">
                    <div className="card-header">
                        <strong>New user</strong>
                    </div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-md-6">
                                <form id="create-user-form">
                                    <div className="form-group">
                                        <label htmlFor="name">First Name</label>
                                        <input type="text" className="form-control" id="name" name="name" onChange={this.onChangeHandler} required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="surname">Surame</label>
                                        <input type="text" className="form-control" id="surname" name="surname" onChange={this.onChangeHandler}required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="gender">Gender</label>
                                        <select type="text" className="form-control" id="gender" name="gender" onChange={this.onChangeHandler} required>
                                            <option value="" selected disabled hidden>Select...</option>
                                            <option >Male</option>
                                            <option>Female</option>
                                            <option>Other</option>
                                        </select>
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="age">Age</label>
                                        <input type="text" className="form-control" id="age" name="age" onChange={this.onChangeHandler} required />
                                    </div>
                                    <button type="submit" className="btn btn-primary" onClick={this.createRtdbRecord}>Submit</button>
                                </form>
                            </div>
                            <div className="col-md-6">
                                {this.state.validation === "failed" &&
                                    <div className="alert alert-danger" role="alert">
                                        Please complete all of the required fields
                                    </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default CreateUser;