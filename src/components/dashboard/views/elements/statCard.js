import React from 'react';

class StatCard extends React.Component {
    render() {
        let iconClass   = `fas ${this.props.iconClass}`;
        let cardClass   = `stat-card-img bg-${this.props.color}`;
        let titleClass  = `card-title text-${this.props.color}`
        return (
            <div className="col-md-4">
                <div className="stat-card">
                    <div className={cardClass}>
                        <i className={iconClass} />
                    </div>
                    <div className="stat-card-body">
                        <h5 className={titleClass}>{this.props.title}</h5>
                        <h6 className="card-subtitle mt-2">
                            {this.props.title === "Total Sales" &&
                                <span>£</span>
                            }
                            {this.props.value}
                            </h6>
                    </div>
                </div>
            </div>
        )
    }
}

export default StatCard;