import React from 'react';

class Cities extends React.Component {

    renderMappedData() {
        //organise by ascending percentage value. Converted to an array as object ordering can be unreliable.

        let sortedArray = [];
        for (var i in this.props.data) {
            sortedArray.push([i, this.props.data[i]]);
        }

        sortedArray.sort(function(a, b) {
            return b[1] - a[1];
        });

        let renderArray = [];

        sortedArray.forEach(function(val) {
            let progBarStyle = {width: `${val[1]}%`};
            renderArray.push(
                <div key={val[0]} className="mt-3">
                    <span className="stat-country-title">{val[0]}</span>
                    <div className="progress">
                        <div className="progress-bar" role="progressbar" style={progBarStyle} aria-valuenow={val[1]} aria-valuemin="0" aria-valuemax="100"></div>
                    </div>
                </div>
            );
        })
        return renderArray;
    }

    render() {
        return (
            <div className="col-md-6">
                <div className="card">
                    <div className="card-body">
                        <h5 className="card-title">Top 5 Consumer Cities</h5>
                        {this.renderMappedData()}
                    </div>
                </div>
            </div>
        )
    }
}

export default Cities;