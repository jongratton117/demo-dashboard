import React from 'react';
import * as firebase from 'firebase';

class StatUpdater extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.createRtdbRecord = this.createRtdbRecord.bind(this);
    }

    onChangeHandler(e) {
        let name = e.target.name;
        let val = e.target.value;
        this.setState({
            [name]: val
        })
    }

    createRtdbRecord(e) {

        // Quick method for checking form validation using inbuilt browser methods. This would normally use a custom validation method and pre-submission confirmation.
        e.preventDefault();
        const form = document.getElementById("update-stat-form")
        form.checkValidity();
        form.reportValidity();

        let updateData = {
            [this.state.stat]: this.state.val
        }
        firebase.database().ref('sample_data/stats').update(updateData);
    }

    render() {
        return (
            <div className="col-md-6">
                <div class="card bg-light mb-3">
                    <div class="card-header">Update Stat Values</div>
                    <div class="card-body">
                        <form id="update-stat-form">
                            <div className="form-group">
                                <label htmlFor="val">New value</label>
                                <input type="number" className="form-control" id="val" name="val" onChange={this.onChangeHandler} required />
                            </div>
                            <div className="form-group">
                                <label htmlFor="stat">Target Stat</label>
                                <select type="stat" className="form-control" id="stat" name="stat" onChange={this.onChangeHandler} required>
                                    <option value="" selected disabled hidden>Select...</option>
                                    <option value="total_sales">Total Sales</option>
                                    <option value="products">Products</option>
                                    <option value="orders">Orders</option>
                                </select>
                            </div>
                            <button type="submit" className="btn btn-primary" onClick={this.createRtdbRecord}>Update</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default StatUpdater;