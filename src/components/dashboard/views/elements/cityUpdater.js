import React from 'react';
import * as firebase from 'firebase';

class CityUpdater extends React.Component {
    constructor(props) {
        super(props);
        this.state = {

        }
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.createRtdbRecord = this.createRtdbRecord.bind(this);
    }

    onChangeHandler(e) {
        let name = e.target.name;
        let val = e.target.value;
        this.setState({
            [name]: val
        })
    }

    createRtdbRecord(e) {

        // Quick method for checking form validation using inbuilt browser methods. This would normally use a custom validation method and pre-submission confirmation.
        e.preventDefault();
        const form = document.getElementById("update-city-form")
        form.checkValidity();
        form.reportValidity();

        let updateData = {
            [this.state.stat]: this.state.val
        }
        // Cheap method for ensuring a new record isn't create with "undefined" value. Would need a more comprehensive solution for all validation.
        if(this.state.stat !== undefined)
            firebase.database().ref('sample_data/countries').update(updateData);
    }


    render() {
        return (
            <div className="col-md-6">
                <div class="card bg-light mb-3">
                    <div class="card-header">Update City Values</div>
                    <div class="card-body">
                        <form id="update-city-form">
                            <div className="form-group">
                                <label htmlFor="val">New value</label>
                                <input type="number" className="form-control" id="val" name="val" onChange={this.onChangeHandler} min="0" max="100" required />
                            </div>
                            <div className="form-group">
                                <label htmlFor="stat">Target Stat</label>
                                <select type="stat" className="form-control" id="stat" name="stat" onChange={this.onChangeHandler} required>
                                    <option value="" selected disabled hidden>Select...</option>
                                    <option value="london">London</option>
                                    <option value="tokyo">Toyko</option>
                                    <option value="manchester">Manchester</option>
                                    <option value="new_york">New York</option>
                                    <option value="madrid">Madrid</option>
                                </select>
                            </div>
                            <button type="submit" className="btn btn-primary" onClick={this.createRtdbRecord}>Update</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    }
}

export default CityUpdater;