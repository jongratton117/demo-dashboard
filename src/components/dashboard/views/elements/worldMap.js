import React from 'react';

const atlas = require('../../../../images/world-map.png');

const worldMap = () => {
    return (
        <div className="col-md-6">
            <div className="card">
                <div className="card-body">
                    <h5 className="card-title">Top 5 Global Consumer Zones</h5>
                    <img src={atlas} alt="World Map" className="world-map" />
                </div>
            </div>
        </div>
    )
}

export default worldMap;