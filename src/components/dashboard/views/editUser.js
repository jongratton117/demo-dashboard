import React            from 'react';
import * as firebase    from 'firebase';
import {Redirect}       from 'react-router-dom';
import * as ROUTES      from '../../../constants/routes';

class EditUser extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            redirect: false
        }
        this.onChangeHandler = this.onChangeHandler.bind(this);
        this.updateRtdbRecord = this.updateRtdbRecord.bind(this);
    }

    componentDidMount() {
        const dbRefObject = firebase.database().ref().child(`users/${this.state.id}`);
        dbRefObject.on('value', snap => {
            this.setState({
                userData:  snap.val()
            })
        });
    }

    onChangeHandler(e) {
        console.log(e.target.value);
        let name = e.target.name;
        let val = e.target.value;
        this.setState({
            newUserData:{
                [name]: val
            }
        })
    }

    async updateRtdbRecord(e) {

        // Quick method for checking form validation using inbuilt browser methods. This would normally use a custom validation method and pre-submission confirmation.
        e.preventDefault();
        const form = document.getElementById("edit-user-form")
        form.checkValidity();
        form.reportValidity();

        if(form.reportValidity() === false)
            this.setState({
                validation: "failed"
            })
        else {
            this.setState({
                validation: "passed"
            })

            //combine objects - ECMAScript 2018 method for quick solution. Would need cross browser support for production build.
            let mergedData = {...this.state.userData, ...this.state.newUserData}

            var updateData = {
                name: mergedData.name,
                surname: mergedData.surname,
                age: mergedData.age,
                gender: mergedData.gender
            }

            await firebase.database().ref('users/' + this.state.id).update(updateData);

            this.setState({
                redirect: true
            })
        }
    }

    render() {
        console.log(this.state.newUserData);
        const redirect = this.state.redirect;
        if(redirect === true)
            return <Redirect to={ROUTES.RTDBDEMO} />
        return (
            <div id="fb-rtdb-page" className="container-fluid">
                <h1 className="section-header">Edit User</h1>
                <p>Edit user data stores in Google Firebase in realtime</p>
                <div className="card">
                    <div className="card-header">
                        <strong>Update user data</strong>
                    </div>
                    <div className="card-body">
                        <div className="row">
                            <div className="col-md-6">
                                {this.state.userData &&
                                <form id="edit-user-form">
                                    <div className="form-group">
                                        <label htmlFor="name">First Name</label>
                                        <input type="text" className="form-control" id="name" name="name" onChange={this.onChangeHandler} required defaultValue={this.state.userData.name} />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="surname">Surame</label>
                                        <input type="text" className="form-control" id="surname" name="surname" onChange={this.onChangeHandler} defaultValue={this.state.userData.surname} required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="gender">Age</label>
                                        <input type="text" className="form-control" id="gender" name="gender" onChange={this.onChangeHandler} defaultValue={this.state.userData.gender} required />
                                    </div>
                                    <div className="form-group">
                                        <label htmlFor="age">Gender</label>
                                        <select className="form-control" id="age" name="age" onChange={this.onChangeHandler} defaultValue={this.state.userData.age} required>
                                            <option>Male</option>
                                            <option>Female</option>
                                            <option>Other</option>
                                        </select>
                                    </div>
                                    <button type="submit" className="btn btn-primary" onClick={this.updateRtdbRecord}>Update</button>
                                </form>
                                }
                            </div>
                            <div className="col-md-6">
                                {this.state.validation === "failed" &&
                                <div className="alert alert-danger" role="alert">
                                    Please complete all of the required fields
                                </div>
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default EditUser;