import React            from 'react';
import {Link}           from 'react-router-dom';
import * as firebase    from 'firebase';
import * as ROUTES      from '../../../constants/routes';

const loading = require('../../../images/loading.svg');

class RtdbDemo extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        }

        this.deleteRtdbRecord = this.deleteRtdbRecord.bind(this);
    }

    componentDidMount() {
        const dbRefObject = firebase.database().ref().child('users');
        dbRefObject.on('value', snap => {
            console.log(snap.val())
            this.setState({
                rtdbObject:  snap.val()
            })
        });
    }

    deleteRtdbRecord(e) {
        const userKey = e.target.dataset.key;
        firebase.database().ref('users/' + userKey).remove();
    }

    renderResponseData() {
        if(this.state.rtdbObject != null) {
            return Object.entries(this.state.rtdbObject).map(([key, value]) => {
                return (
                    <tr key={key}>
                        <td>{value.name}</td>
                        <td>{value.surname}</td>
                        <td>{value.gender}</td>
                        <td>{value.age}</td>
                        <td>
                            <div className="dropdown">
                                <button className="btn btn-outline-dark btn-sm dropdown-toggle" type="button" id="entry-actions" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i className="fas fa-cog" />
                                </button>
                                <div className="dropdown-menu" aria-labelledby="entry-actions">
                                    <Link to={ROUTES.makeCreateUserPath(key)} params={{id: key}} className="dropdown-item">Edit</Link>
                                    <button className="dropdown-item text-danger" type="button" data-key={key} onClick={this.deleteRtdbRecord}>Delete</button>
                                </div>
                            </div>
                        </td>
                    </tr>
                )
            })
        } else {
            return (
                <tr>
                    <td colSpan="5">
                        Fetching data <img className="loading-icon" src={loading} alt="" />
                    </td>
                </tr>
            )
        }
    }

    render() {
        return (
            <div id="fb-rtdb-page" className="container-fluid">
                <h1 className="section-header">User Management</h1>
                <p className="sub-header">A simple example of a user management interface that updates in real time with Firebase database.</p>
                <div className="card" >
                    <div className="card-header">
                        <strong>USERS</strong>
                    </div>
                    <div className="card-body">
                        <div className="table-responsive">
                            <table className="table">
                                <thead className="thead-light">
                                <tr>
                                    <th>Name</th>
                                    <th>Surname</th>
                                    <th>Gender</th>
                                    <th>Age</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                    {this.renderResponseData()}
                                </tbody>
                            </table>
                        </div>
                        <Link to={ROUTES.CREATE_USER} className="btn btn-primary mb-3 mt-3"><i className="fas fa-plus" /> Create new user</Link>
                    </div>
                </div>
            </div>
        )
    }
}

export default RtdbDemo;