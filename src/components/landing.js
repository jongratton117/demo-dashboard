import React    from 'react';
import {Link}   from 'react-router-dom';
import * as ROUTES from '../constants/routes';

const Landing = () => {
    return (
        <div id="landing">
            <div className="content-box content-box-center">
                <h1>Demo Dashboard</h1>
                <p>This is an app developed to demonstrate the design and construction of a <strong>React Application</strong>.</p>
                <p>It simulates a dashboard environment that could be used as part of an operations tool.</p>
                <p>It's built on a <strong>React boilerplate</strong> and connects to a <strong>Google Firebase Realtime Database</strong> to store and edit data.</p>
                <Link to={ROUTES.DASHBOARD} class="btn btn-primary">Proceed</Link>
            </div>
        </div>
    )
}

export default Landing;